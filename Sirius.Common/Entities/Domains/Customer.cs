﻿using Sirius.Core.Models;
using System.Collections.Generic;

namespace Sirius.Common.Entities.Domains
{
    public class Customer : EntityBase, IPkEntity<long>
    {
        
        public long Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
        public long? PrimaryContactId { get; set; }
        public bool Active { get; set; }
        //public DateTime CreatedDate { get; set; }
        //public decimal Potential { get; set; }
        //public bool Active { get; set; }

        //public virtual IList<Bill> Bills { get; set; }
        public virtual ICollection<Contact> Contacts { get; set; }
    }
}
