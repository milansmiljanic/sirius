﻿using Sirius.Core.Models;
using System;

namespace Sirius.Common.Entities.Domains
{
    public class Contact : EntityBase, IPkEntity<long>
    {
        public Contact()
        {
            //DocumentHeaders = new HashSet<DocumentHeaders>();
        }

        public long Id { get; set; }
        public string ContactBusinessKey { get; set; }
        public int? FinancialEntityId { get; set; }
        public string Code { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Mobile { get; set; }
        public string Email { get; set; }
        public long CustomerId { get; set; }
        public int? ContactDepartmentId { get; set; }
        public int? ContactPositionId { get; set; }
        public int? EmployeeId { get; set; }
        public string Address { get; set; }
        public int? PostalCodeAndCityId { get; set; }
        public bool? PrimarContactAtCustomer { get; set; }
        public int? CreatedByUserId { get; set; }
        public DateTime ModifiedDate { get; set; }
        public int? AuditId { get; set; }
        public int ModifiedByUserId { get; set; }
        public bool Active { get; set; }
        public string CustBusinessUnitCode { get; set; }
        public string Phone { get; set; }

        public virtual Customer Customer { get; set; }        
    }
}
