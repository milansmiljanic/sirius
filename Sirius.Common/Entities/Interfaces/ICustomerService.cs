﻿using Sirius.Common.Entities.Domains;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Sirius.Common.Entities.Interfaces
{
    public interface ICustomerService
    {
        Task<IEnumerable<Customer>> GetAllAsync();

        Task<IEnumerable<Customer>> GetByIdAsync(long customerId);

        Task<Customer> UpdateCustomerAsync(long customerId);
    }
}
