﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Sirius.Core.Models
{
    public interface IPkEntity<T>
    {
        T Id { get; set; }
    }
}
