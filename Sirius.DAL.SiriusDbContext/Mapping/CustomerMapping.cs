﻿using Sirius.Common.Entities.Domains;
using System.Data.Entity.ModelConfiguration;

namespace Sirius.DAL.SiriusDbContext.Mapping
{
    public class CustomerMapping : EntityTypeConfiguration<Customer>
    {
        public CustomerMapping()
        {
            this.ToTable("Customers");
            this.Property(p => p.Id).HasColumnName("CustomerID");
        }
    }
}
