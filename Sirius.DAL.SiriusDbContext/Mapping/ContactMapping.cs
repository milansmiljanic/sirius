﻿using Sirius.Common.Entities.Domains;
using System.Data.Entity.ModelConfiguration;

namespace Sirius.DAL.SiriusDbContext.Mapping
{
    public class ContactMapping : EntityTypeConfiguration<Contact>
    {
        public ContactMapping()
        {
            HasKey(e => e.Id);

            ToTable("Contacts");

            HasIndex(e => new { e.FinancialEntityId, e.ContactBusinessKey })
                .HasName("Contacts_FE_CBK");

            Property(e => e.Id).HasColumnName("ContactID");

            Property(e => e.Active);

            Property(e => e.Address).HasMaxLength(250);

            Property(e => e.AuditId)
                .HasColumnName("AuditID");

            Property(e => e.Code).HasMaxLength(50);

            Property(e => e.ContactBusinessKey).HasMaxLength(150);

            Property(e => e.ContactDepartmentId).HasColumnName("ContactDepartmentID");

            Property(e => e.ContactPositionId).HasColumnName("ContactPositionID");

            Property(e => e.CreatedByUserId).HasColumnName("CreatedByUserID");

            Property(e => e.CustBusinessUnitCode).HasMaxLength(50);

            Property(e => e.CustomerId).HasColumnName("CustomerID");

            Property(e => e.Email).HasMaxLength(100);

            Property(e => e.EmployeeId).HasColumnName("EmployeeID");

            Property(e => e.FinancialEntityId).HasColumnName("FinancialEntityID");

            Property(e => e.FirstName).HasMaxLength(150);

            Property(e => e.LastName).HasMaxLength(150);

            Property(e => e.Mobile).HasMaxLength(50);

            Property(e => e.ModifiedByUserId).HasColumnName("ModifiedByUserID");

            Property(e => e.ModifiedDate).HasColumnType("datetime");

            Property(e => e.PostalCodeAndCityId).HasColumnName("PostalCodeAndCityID");

            Property(e => e.PrimarContactAtCustomer);

            Property(e => e.Phone).HasMaxLength(50);

            HasRequired(e => e.Customer)
                .WithMany(p => p.Contacts)
                    .HasForeignKey(d => d.CustomerId); 
        }
    }
}
