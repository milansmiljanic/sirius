﻿using Sirius.Common.Entities.Domains;
using Sirius.DAL.Common.Context;
using Sirius.DAL.SiriusDbContext.Mapping;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace Sirius.DAL.SiriusDbContext
{
    public class SiriusDbContext : EntityContextBase<SiriusDbContext>
    {
        public SiriusDbContext() : base("name=Sirius-devConnectionString")
        {            
            //Disable initializer
            Database.SetInitializer<SiriusDbContext>(null);

            Configuration.LazyLoadingEnabled = false;
            //Configuration.ProxyCreationEnabled = false;
        }

        public DbSet<Customer> Customers { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            modelBuilder.Configurations.Add(new CustomerMapping());
            modelBuilder.Configurations.Add(new ContactMapping());
        }
    }
}
