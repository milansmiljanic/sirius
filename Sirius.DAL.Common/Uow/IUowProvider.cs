﻿using Sirius.DAL.Common.Context;

namespace Sirius.DAL.Common.Uow
{
    public interface IUowProvider
    {
        IUnitOfWork CreateUnitOfWork<TEntityContext>() where TEntityContext : IEntityContext;        
    }
}
