﻿using Sirius.DAL.Common.Repositories;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Sirius.DAL.Common.Uow
{
    public interface IUnitOfWorkBase : IDisposable
    {
        int SaveChanges();
        Task<int> SaveChangesAsync();
        Task<int> SaveChangesAsync(CancellationToken cancellationToken);

        IRepository<TEntity, TId> GetRepository<TEntity, TId>();
        TRepository GetCustomRepository<TRepository>();
    }
}
