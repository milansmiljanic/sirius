﻿using Sirius.DAL.Common.Context;
using System;
using System.Data.Entity;

namespace Sirius.DAL.Common.Uow
{
    public class UowProvider : IUowProvider
    {
        public UowProvider()
        { }

        public UowProvider(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        private readonly IServiceProvider _serviceProvider;
        private static readonly object _lock = new object();

        public IUnitOfWork CreateUnitOfWork<TEntityContext>() where TEntityContext : IEntityContext
        {
            lock (_lock)
            {
                var _context = (DbContext)_serviceProvider.GetService(typeof(TEntityContext));

                var uow = new UnitOfWork(_context, _serviceProvider);
                return uow;
            }
        }
    }
}
