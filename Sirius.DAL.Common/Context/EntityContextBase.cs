﻿using System.Data.Entity;

namespace Sirius.DAL.Common.Context
{
    public class EntityContextBase<TContext> : DbContext, IEntityContext where TContext : DbContext
    {
        public EntityContextBase(string connectionString) : base(connectionString)
        {

        }
    }
}
