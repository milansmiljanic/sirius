﻿using System.Data.Entity;

namespace Sirius.DAL.Common.Repositories
{
    public interface IRepositoryInjection<TContext> where TContext : DbContext
    {
        IRepositoryInjection<TContext> SetContext(TContext context);
    }
}
