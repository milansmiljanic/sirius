﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Sirius.DAL.Common.Query;
using System.Threading;
using System.Data.Entity;
using Sirius.Core.Models;

namespace Sirius.DAL.Common.Repositories
{
    public abstract class EntityRepositoryBase<TContext, TEntity, TId> : RepositoryBase<TContext>, IRepository<TEntity, TId> where TContext : DbContext where TEntity : EntityBase, IPkEntity<TId>, new() where TId : IComparable
    {
        private readonly OrderBy<TEntity> DefaultOrderBy = new OrderBy<TEntity>(qry => qry.OrderBy(e => e.Id));
        private static readonly object _lock = new object();
        private static SemaphoreSlim SemaphoreSlim = new SemaphoreSlim(1, 1);

        protected EntityRepositoryBase(TContext context) : base(context)
        { }

        public virtual IEnumerable<TEntity> GetAll(Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null, Func<IQueryable<TEntity>, IQueryable<TEntity>> includes = null)
        {
            lock (_lock)
            {
                var filter = new Filter<TEntity>();
                var result = QueryDb(filter.Expression, orderBy, includes);
                return result.ToList();
            }
        }

        public virtual async Task<IEnumerable<TEntity>> GetAllAsync(Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null, Func<IQueryable<TEntity>, IQueryable<TEntity>> includes = null)
        {
            await SemaphoreSlim.WaitAsync();
            try
            {
                var resultDb = QueryDb(null, orderBy, includes);
                var result = await resultDb.ToListAsync();
                return result;
            }
            finally
            {
                SemaphoreSlim.Release();
            }
        }

        public virtual void Load(Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null, Func<IQueryable<TEntity>, IQueryable<TEntity>> includes = null)
        {
            lock (_lock)
            {
                var result = QueryDb(null, orderBy, includes);
                result.AsNoTracking().Load();
            }
        }

        public virtual async Task LoadAsync(Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null, Func<IQueryable<TEntity>, IQueryable<TEntity>> includes = null)
        {
            await SemaphoreSlim.WaitAsync();
            try
            {
                var result = QueryDb(null, orderBy, includes);
                await result.AsNoTracking().LoadAsync();
            }
            finally
            {
                SemaphoreSlim.Release();
            }
        }

        public virtual IEnumerable<TEntity> GetPage(int startRow, int pageLength, Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null, Func<IQueryable<TEntity>, IQueryable<TEntity>> includes = null)
        {
            lock (_lock)
            {
                if (orderBy == null) orderBy = DefaultOrderBy.Expression;

                var dbResult = QueryDb(null, orderBy, includes);
                var pagedList = dbResult.Skip(startRow).Take(pageLength).AsNoTracking().ToList();

                var result = orderBy(pagedList.AsQueryable()).ToList();
                return result;
            }
        }

        public virtual async Task<IEnumerable<TEntity>> GetPageAsync(int startRow, int pageLength, Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null, Func<IQueryable<TEntity>, IQueryable<TEntity>> includes = null)
        {
            await SemaphoreSlim.WaitAsync();
            try
            {
                if (orderBy == null) orderBy = DefaultOrderBy.Expression;

                var dbResult = QueryDb(null, orderBy, includes);
                var pagedList = await dbResult.Skip(startRow).Take(pageLength).AsNoTracking().ToListAsync();

                var result = orderBy(pagedList.AsQueryable()).ToList();
                return result;
            }
            finally
            {
                SemaphoreSlim.Release();
            }
        }

        public virtual TEntity Get(TId id, Func<IQueryable<TEntity>, IQueryable<TEntity>> includes = null)
        {
            lock (_lock)
            {
                IQueryable<TEntity> query = Context.Set<TEntity>();

                if (includes != null)
                {
                    query = includes(query);
                }

                return query.AsNoTracking().SingleOrDefault(x => x.Id.Equals(id));
            }
        }

        public virtual TEntity GetForUpdate(TId id, Func<IQueryable<TEntity>, IQueryable<TEntity>> includes = null)
        {
            lock (_lock)
            {
                IQueryable<TEntity> query = Context.Set<TEntity>();

                if (includes != null)
                {
                    query = includes(query);
                }

                return query.SingleOrDefault(x => x.Id.Equals(id));
            }
        }

        public virtual async Task<TEntity> GetAsync(TId id, Func<IQueryable<TEntity>, IQueryable<TEntity>> includes = null)
        {
            await SemaphoreSlim.WaitAsync();
            try
            {
                IQueryable<TEntity> query = Context.Set<TEntity>();

                if (includes != null)
                {
                    query = includes(query);
                }

                return await query.AsNoTracking().SingleOrDefaultAsync(x => x.Id.Equals(id));
            }
            finally
            {
                SemaphoreSlim.Release();
            }
        }

        public virtual async Task<TEntity> GetForUpdateAsync(TId id, Func<IQueryable<TEntity>, IQueryable<TEntity>> includes = null)
        {
            await SemaphoreSlim.WaitAsync();
            try
            {
                IQueryable<TEntity> query = Context.Set<TEntity>();

                if (includes != null)
                {
                    query = includes(query);
                }

                return await query.SingleOrDefaultAsync(x => x.Id.Equals(id));
            }
            finally
            {
                SemaphoreSlim.Release();
            }
        }

        public virtual IEnumerable<TEntity> Query(Expression<Func<TEntity, bool>> filter, Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null, Func<IQueryable<TEntity>, IQueryable<TEntity>> includes = null)
        {
            lock (_lock)
            {
                var result = QueryDb(filter, orderBy, includes);
                return result.AsNoTracking().ToList();
            }
        }

        public virtual async Task<IEnumerable<TEntity>> QueryAsync(Expression<Func<TEntity, bool>> filter, Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null, Func<IQueryable<TEntity>, IQueryable<TEntity>> includes = null)
        {
            await SemaphoreSlim.WaitAsync();
            try
            {
                var result = QueryDb(filter, orderBy, includes);
                return await result.AsNoTracking().ToListAsync();
            }
            finally
            {
                SemaphoreSlim.Release();
            }
        }

        public virtual IEnumerable<TEntity> QueryForUpdate(Expression<Func<TEntity, bool>> filter, Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null, Func<IQueryable<TEntity>, IQueryable<TEntity>> includes = null)
        {
            lock (_lock)
            {
                var result = QueryDb(filter, orderBy, includes);
                return result.ToList();
            }
        }

        public virtual async Task<IEnumerable<TEntity>> QueryForUpdateAsync(Expression<Func<TEntity, bool>> filter, Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null, Func<IQueryable<TEntity>, IQueryable<TEntity>> includes = null)
        {
            await SemaphoreSlim.WaitAsync();
            try
            {
                var result = QueryDb(filter, orderBy, includes);
                return await result.ToListAsync();
            }
            finally
            {
                SemaphoreSlim.Release();
            }
        }

        public virtual void Load(Expression<Func<TEntity, bool>> filter, Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null, Func<IQueryable<TEntity>, IQueryable<TEntity>> includes = null)
        {
            lock (_lock)
            {
                var result = QueryDb(filter, orderBy, includes);
                result.AsNoTracking().Load();
            }
        }

        public virtual async Task LoadAsync(Expression<Func<TEntity, bool>> filter, Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null, Func<IQueryable<TEntity>, IQueryable<TEntity>> includes = null)
        {
            await SemaphoreSlim.WaitAsync();
            try
            {
                var result = QueryDb(filter, orderBy, includes);
                await result.AsNoTracking().LoadAsync();
            }
            finally
            {
                SemaphoreSlim.Release();
            }
        }

        public virtual IEnumerable<TEntity> QueryPage(int startRow, int pageLength, Expression<Func<TEntity, bool>> filter, Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null, Func<IQueryable<TEntity>, IQueryable<TEntity>> includes = null)
        {
            lock (_lock)
            {
                if (orderBy == null) orderBy = DefaultOrderBy.Expression;

                var dbResult = QueryDb(filter, orderBy, includes);
                var pagedList = dbResult.Skip(startRow).Take(pageLength).AsNoTracking().ToList();

                var result = orderBy(pagedList.AsQueryable()).ToList();
                return result;
            }
        }

        public virtual async Task<IEnumerable<TEntity>> QueryPageAsync(int startRow, int pageLength, Expression<Func<TEntity, bool>> filter, Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null, Func<IQueryable<TEntity>, IQueryable<TEntity>> includes = null)
        {
            await SemaphoreSlim.WaitAsync();
            try
            {
                if (orderBy == null) orderBy = DefaultOrderBy.Expression;

                var dbResult = QueryDb(filter, orderBy, includes);
                var pagedList = await dbResult.Skip(startRow).Take(pageLength).AsNoTracking().ToListAsync();

                var result = orderBy(pagedList.AsQueryable()).ToList();
                return result;
            }
            finally
            {
                SemaphoreSlim.Release();
            }
        }

        public virtual void Add(TEntity entity)
        {
            lock (_lock)
            {
                if (entity == null) throw new InvalidOperationException("Unable to add a null entity to the repository.");
                Context.Set<TEntity>().Add(entity);
            }
        }

        public virtual void Update(TEntity entity)
        {
            lock (_lock)
            {
                Context.Set<TEntity>().Attach(entity);
                Context.Entry(entity).State = EntityState.Modified;
            }
        }

        public virtual void Remove(TEntity entity)
        {
            lock (_lock)
            {
                Context.Set<TEntity>().Attach(entity);
                Context.Entry(entity).State = EntityState.Deleted;
                Context.Set<TEntity>().Remove(entity);
            }
        }

        public virtual void Remove(TId id)
        {
            lock (_lock)
            {
                var entity = new TEntity() { Id = id };
                this.Remove(entity);
            }
        }

        public virtual bool Any(Expression<Func<TEntity, bool>> filter = null)
        {
            lock (_lock)
            {
                IQueryable<TEntity> query = Context.Set<TEntity>();

                if (filter != null)
                {
                    query = query.Where(filter);
                }

                return query.AsNoTracking().Any();
            }
        }

        public virtual int Max(Expression<Func<TEntity, int>> id)
        {
            lock (_lock)
            {
                IQueryable<TEntity> query = Context.Set<TEntity>();
                var maxId = query.AsNoTracking().Max(id);
                return maxId;
            }
        }

        public virtual long Max(Expression<Func<TEntity, long>> id)
        {
            lock (_lock)
            {
                IQueryable<TEntity> query = Context.Set<TEntity>();
                var maxId = query.AsNoTracking().Max(id);
                return maxId;
            }
        }

        public virtual async Task<int> MaxAsync(Expression<Func<TEntity, int>> id)
        {
            await SemaphoreSlim.WaitAsync();
            try
            {
                IQueryable<TEntity> query = Context.Set<TEntity>();
                return await query.AsNoTracking().MaxAsync(id);
            }
            finally
            {
                SemaphoreSlim.Release();
            }
        }

        public virtual async Task<long> MaxAsync(Expression<Func<TEntity, long>> id)
        {
            IQueryable<TEntity> query = Context.Set<TEntity>();
            return await query.AsNoTracking().MaxAsync(id);
        }

        public virtual async Task<bool> AnyAsync(Expression<Func<TEntity, bool>> filter = null)
        {
            await SemaphoreSlim.WaitAsync();
            try
            {
                IQueryable<TEntity> query = Context.Set<TEntity>();

                if (filter != null)
                {
                    query = query.Where(filter);
                }

                return await query.AsNoTracking().AnyAsync();
            }
            finally
            {
                SemaphoreSlim.Release();
            }
        }

        public virtual int Count(Expression<Func<TEntity, bool>> filter = null)
        {
            lock (_lock)
            {
                IQueryable<TEntity> query = Context.Set<TEntity>();

                if (filter != null)
                {
                    query = query.Where(filter);
                }

                return query.AsNoTracking().Count();
            }
        }

        public virtual async Task<int> CountAsync(Expression<Func<TEntity, bool>> filter = null)
        {
            await SemaphoreSlim.WaitAsync();
            try
            {
                IQueryable<TEntity> query = Context.Set<TEntity>();

                if (filter != null)
                {
                    query = query.Where(filter);
                }

                return await query.AsNoTracking().CountAsync();
            }
            finally
            {
                SemaphoreSlim.Release();
            }
        }

        protected IQueryable<TEntity> QueryDb(Expression<Func<TEntity, bool>> filter, Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy, Func<IQueryable<TEntity>, IQueryable<TEntity>> includes)
        {
            IQueryable<TEntity> query = Context.Set<TEntity>();

            if (filter != null)
            {
                query = query.Where(filter);
            }

            if (includes != null)
            {
                query = includes(query);
            }

            if (orderBy != null)
            {
                query = orderBy(query);
            }

            return query;
        }

        public void SetUnchanged(TEntity entity)
        {
            base.Context.Entry<TEntity>(entity).State = EntityState.Unchanged;
        }

        private T ConvertToType<T>(object value)
        {
            var converted = (T)(Convert.ChangeType(value, typeof(T)));
            return converted;
        }
    }
}
