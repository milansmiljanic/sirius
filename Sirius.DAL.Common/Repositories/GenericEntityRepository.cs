﻿using Sirius.Core.Models;
using System;
using System.Data.Entity;

namespace Sirius.DAL.Common.Repositories
{
    public class GenericEntityRepository<TEntity, TId> : EntityRepositoryBase<DbContext, TEntity, TId> where TEntity : EntityBase, IPkEntity<TId>, new() where TId : IComparable
    {
        public GenericEntityRepository() : base(null)
        { }
    }
}
