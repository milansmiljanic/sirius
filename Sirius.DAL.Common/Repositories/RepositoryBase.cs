﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace Sirius.DAL.Common.Repositories
{
    public abstract class RepositoryBase<TContext> : IRepositoryInjection<TContext> where TContext : DbContext
    {
        protected RepositoryBase(TContext context)
        {
            this.Context = context;
        }

        protected TContext Context { get; private set; }

        public virtual IRepositoryInjection<TContext> SetContext(TContext context)
        {
            this.Context = context;
            return this;
        }
    }
}
