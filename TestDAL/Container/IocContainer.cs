﻿using System.Web.Mvc;
using Castle.Windsor;
using Castle.Windsor.Installer;
using Sirius.Ioc.Container;

namespace TestDAL.Container
{
    public static class IocContainer
    {
        private static IWindsorContainer _container;

        public static void Setup()
        {
            _container = new WindsorContainer().Install(FromAssembly.Containing<BusinessLogicInstaller>()).Install(FromAssembly.This());

            var controllerFactory = new WindsorControllerFactory(_container.Kernel);
            ControllerBuilder.Current.SetControllerFactory(controllerFactory);
        }

        public static void Dispose()
        {
            _container.Dispose();
        }
    }
}