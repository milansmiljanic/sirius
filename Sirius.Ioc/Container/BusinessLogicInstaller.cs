﻿using Castle.MicroKernel.Facilities;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using Sirius.Common.Entities.Interfaces;
using Sirius.DAL.Common.Context;
using Sirius.DAL.Common.Repositories;
using Sirius.DAL.Common.Uow;
using Sirius.DAL.SiriusDbContext;
using Sirius.Services;
using System;

namespace Sirius.Ioc.Container
{
    public class BusinessLogicInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(Component.For<IWindsorContainer>().Instance(container));

            container.Register(Component.For<ICustomerService>().ImplementedBy<CustomerService>().LifestylePerWebRequest());

            container.Register(Component.For<IUowProvider>().ImplementedBy<UowProvider>().LifestylePerWebRequest());
            container.Register(Component.For<IServiceProvider>().ImplementedBy<WindsorServiceProvider>().LifestylePerWebRequest());


            container.AddFacility<EntityFrameworkFacility>();
        }

        public class EntityFrameworkFacility : AbstractFacility
        {
            protected override void Init()
            {
                Kernel.Register(Component.For<IEntityContext>()
                                         .ImplementedBy<SiriusDbContext>()
                                         .LifestyleTransient(),
                                Component.For(typeof(IRepository<,>))
                                         .ImplementedBy(typeof(GenericEntityRepository<,>))
                                         .LifestyleTransient());
            }
        }

        public class WindsorServiceProvider : IServiceProvider
        {
            private IWindsorContainer _container;

            public WindsorServiceProvider(IWindsorContainer container)
            {
                _container = container;
            }

            public object GetService(Type serviceType)
            {
                return _container.Resolve(serviceType);
            }
        }
    }
}

