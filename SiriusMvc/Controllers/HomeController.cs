﻿using Sirius.Common.Entities.Interfaces;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace SiriusMvc.Controllers
{
    public class HomeController : Controller
    {
        private readonly ICustomerService _customerService;

        public HomeController(ICustomerService customerService)
        {
            _customerService = customerService;
        }
        public async Task<ActionResult> Index()
        {
            var resutt = await _customerService.GetAllAsync();

            var res = await _customerService.GetByIdAsync(43613);

            var cust = await _customerService.UpdateCustomerAsync(43613);

            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}