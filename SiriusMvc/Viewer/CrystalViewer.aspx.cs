﻿using CrystalDecisions.CrystalReports.Engine;
using SiriusMvc.Controllers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LearnMvcClient.Viewer
{
    public partial class CrystalViewer : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void CrystalReportViewer1_Load(object sender, EventArgs e)
        {http://localhost:3496/Viewer/CrystalViewer.aspx.cs
            //Bind ReportSource to crystalReportViewer
            CrystalReportViewer1.ReportSource = GetReportDucument();
        }

        private object GetReportDucument()
        {
            ReportDocument rd = new ReportDocument();
            rd.Load(Path.Combine(Server.MapPath("~/Reports"), "CrystalReport1.rpt"));
            rd.SetDataSource(FlexGridController.Customers);

            return rd;
        }
    }
}