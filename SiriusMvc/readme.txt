﻿ASP.NET MVC Learn MVC Client
-----------------------
A catalog of samples that show C1 MVC client features.

This project shows how to use the TreeView control for navigation,
and how to style it using some cool CSS techniques.

This version includes samples for following modules:
- MVC (core)
- Input
- FlexGrid
- FlexChart
- Gauge
- Nav
