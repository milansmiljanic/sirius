﻿using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using System.Web.Mvc;

namespace SiriusMvc.Container
{
    public class ControllersInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(Classes.FromThisAssembly()
                                   .BasedOn<IController>()
                                   .If(t => t.Name.EndsWith("Controller"))
                                   .LifestylePerWebRequest());
            //container.Register(AllTypes.FromThisAssembly()
            //                       .BasedOn<IController>()
            //                       .If(t => t.Name.EndsWith("Controller"))
            //                       .Configure(_ => _.LifeStyle.PerWebRequest));
        }
    }
}