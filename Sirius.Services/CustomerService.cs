﻿using Sirius.Common.Entities.Domains;
using Sirius.Common.Entities.Interfaces;
using Sirius.DAL.Common.Context;
using Sirius.DAL.Common.Uow;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data.Entity;

namespace Sirius.Services
{
    public class CustomerService : ICustomerService
    {
        private readonly IUowProvider _uowProvider;

        public CustomerService(IUowProvider uowProvider)
        {
            _uowProvider = uowProvider;
        }

        public async Task<IEnumerable<Customer>> GetAllAsync()
        {
            using (var uow = _uowProvider.CreateUnitOfWork<IEntityContext>())
            {
                var connectedRepo = uow.GetRepository<Customer, long>();

                Func<IQueryable<Customer>, IQueryable<Customer>> includes = query =>
                {
                    return query
                        .Include(c => c.Contacts);
                };
                var result = await connectedRepo.QueryAsync(x => x.Active, null, includes);
                
                return result;
            }
        }

        public async Task<IEnumerable<Customer>> GetByIdAsync(long customerId)
        {
            using (var uow = _uowProvider.CreateUnitOfWork<IEntityContext>())
            {
                var repo = uow.GetRepository<Customer, long>();

                Func<IQueryable<Customer>, IQueryable<Customer>> includes = query =>
                {
                    return query
                        .Include(c => c.Contacts);
                };
                var result = await repo.QueryForUpdateAsync(x => x.Active && x.Id == customerId, null, includes);

                return result;                
            }
        }

        public async Task<Customer> UpdateCustomerAsync(long customerId)
        {
            using (var uow = _uowProvider.CreateUnitOfWork<IEntityContext>())
            {
                var repo = uow.GetRepository<Customer, long>();

                Func<IQueryable<Customer>, IQueryable<Customer>> includes = query =>
                {
                    return query
                        .Include(c => c.Contacts);
                };
                var result = (await repo.QueryForUpdateAsync(x => x.Active && x.Id == customerId, null, includes)).First();
                result.Name = "Milan Smilj";
                result.Phone = "test phone num";

                repo.Update(result);
                await uow.SaveChangesAsync();

                return result;
            }
        }
    }
}
